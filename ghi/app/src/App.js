import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import React from 'react';
import { useState, useEffect } from "react";
import HatsList from './HatsList';
import HatsForm from './HatsForm'

function App(props) {
  const [shoes, setShoes] = useState([])
  const [hats, setHats] = useState([])

  const getShoes = async () => {
    const url = 'http://localhost:8080/api/shoes/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const shoes = data.shoes
      setShoes(shoes)
    }
  }

  const loadHats = async () => {
    const response = await fetch('http://localhost:8090/api/hats/');
    if (response.ok) {
      const data = await response.json();
      const hats = data.hats
      setHats(hats)
    }}

  useEffect(() => {
    getShoes();
    loadHats();
  }, [])

return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats/" element={<HatsList hats={hats} loadHats={loadHats}/>} />
          <Route path="hats/">
          <Route path="new" element={<HatsForm loadHats={loadHats}/>} />
          </Route>
          <Route path="" element={<MainPage />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path="/shoes" element={<ShoeList shoes={shoes} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
