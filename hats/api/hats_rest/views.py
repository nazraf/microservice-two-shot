from django.shortcuts import render
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hats, LocationVO

# Create your views here.
# ModelEncoder is a class in the Django REST Framework that provides a way to serialize models to Python primitives. It's essentially a tool that allows you to convert data from a Django model instance into a format that can be easily sent over the web, such as JSON.

# When you define a custom ModelEncoder class, you can specify which fields of the model should be serialized and how they should be serialized. For example, you might want to exclude certain fields from the output, or you might want to format dates in a specific way.

# Custom ModelEncoder classes are often used in conjunction with the JsonResponse class in Django to create HTTP responses containing serialized model data.


# This line defines a new class called LocationVODetailEncoder that inherits from ModelEncoder.
class LocationVODetailEncoder(ModelEncoder):
    # his line specifies the model that this encoder is designed to encode, which is LocationVO.
    model = LocationVO
    # This line specifies the properties of the LocationVO model that should be included in the encoded output. Specifically, the closet_name, section_number, shelf_number, and import_href properties will be included.
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style",
        "color",
        "url_picture",
        "id",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style",
        "color",
        "url_picture",
        "id",
        "location",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


# This checks if the HTTP request method is GET. If it is, then the function retrieves all objects of the Hats model using Hats.objects.all(), creates a JSON response containing a dictionary with a single key "hats" and the value of hats, and returns the response. The response is encoded using the HatsListEncoder and is marked as unsafe for non-dict objects using the safe=False parameter.
@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatsListEncoder, safe=False)

    # If the HTTP request method is not GET, then the function assumes that it is a POST request and attempts to create a new object of the Hats model using the Hats.objects.create(**content) method, where content is the JSON-encoded request body. If the location field in content contains a valid LocationVO object, then it is replaced with the actual LocationVO object. If the location field does not contain a valid LocationVO object, then the function returns a JSON response with an error message and a status code of 400. The response is encoded using the HatsDetailEncoder and is marked as unsafe for non-dict objects using the safe=False parameter.

    # {
    #         "locations": [
    #             {
    #             "href": "/api/locations/2/",
    #             "id": 2,
    #             "closet_name": "Josh's Closet",
    #             "section_number": 3,
    #             "shelf_number": 2
    #             }
    #         ]
    #     }

    else:  # POST method
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            # "href": "/api/locations/2/",
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
    hat = Hats.objects.create(**content)
    return JsonResponse(
        hat,
        encoder=HatsDetailEncoder,
        safe=False,
    )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_hat_details(request, pk):
    if request.method == "GET":
        hats = Hats.objects.get(id=pk)
        return JsonResponse(hats, encoder=HatsListEncoder, safe=False)
    else:
        request.method == "DELETE"
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
