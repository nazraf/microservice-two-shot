from django.db import models
from django.urls import reverse




# Create your models here.
class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, null=True, unique=True)

    # This defines a method get_api_url that returns a URL for the API endpoint for this LocationVO object. The URL is constructed using the reverse function with the name of the view function ("api_location") and the primary key ("pk") of the object as the keyword argument.
    def get_api_url(self):
        return reverse("api_location", kwargs={"pk": self.pk})


    # This defines a method __str__ that returns a string representation of the LocationVO object. The string is constructed by concatenating the closet_name, section_number, and shelf_number fields separated by a hyphen.
    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"


    # This defines a nested class Meta that specifies the default ordering of LocationVO objects. The ordering is based on the closet_name, section_number, and shelf_number fields, in that order.
    class Meta:
        ordering = ("closet_name", "section_number", "shelf_number")

class Hats(models.Model):
    fabric = models.CharField(max_length=250)
    style = models.CharField(max_length=250)
    color = models.CharField(max_length=250)
    url_picture = models.URLField(null=True)

    # This line defines a foreign key field called location that references the LocationVO model. The related_name argument specifies the name of the reverse relation from LocationVO to Hats (i.e., the name of the attribute that holds the set of Hats objects that are associated with a particular LocationVO). The on_delete argument specifies what should happen when the referenced LocationVO object is deleted. In this case, when a LocationVO object is deleted, all associated Hats objects will also be deleted.
    location = models.ForeignKey(
        LocationVO,
        related_name='location',
        on_delete=models.CASCADE,
    )
